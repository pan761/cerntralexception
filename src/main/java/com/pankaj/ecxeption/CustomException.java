package com.pankaj.ecxeption;

import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
//@Component 
public class CustomException extends Exception {

	/**
	 * 
	 */
	//private static final long serialVersionUID = -8584743640237759883L;
	private int code;
	private String messages;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessages() {
		return messages;
	}

	public void setMessages(String messages) {
		this.messages = messages;
	}

}
