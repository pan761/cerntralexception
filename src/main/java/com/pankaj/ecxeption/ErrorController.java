package com.pankaj.ecxeption;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ErrorController extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ExceptionResponse> generalEx(Exception e) throws Exception{
		
		
		ExceptionResponse er= new ExceptionResponse();
		er.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		er.setDescription(e.getMessage());
		System.out.println(e);
		return new ResponseEntity<>(er,HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	
	@ExceptionHandler(CustomException.class)
	public ResponseEntity<ExceptionResponse> specialEx(CustomException e) throws Exception{
		
		
		ExceptionResponse er= new ExceptionResponse();
		er.setCode(e.getCode());
		er.setDescription(e.getMessages());
		System.out.println(e);
		return new ResponseEntity<>(er,HttpStatus.INTERNAL_SERVER_ERROR);
		
	}
	
	protected ResponseEntity<ExceptionResponse> handleMethodArgumentNotValid(Exception e){
		
		ExceptionResponse er= new ExceptionResponse();
		er.setCode(300);
		return new ResponseEntity<>(er,HttpStatus.INTERNAL_SERVER_ERROR);
		
	}
}
