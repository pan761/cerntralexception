package com.pankaj.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.pankaj.ecxeption.CustomException;
import com.pankaj.service.TestService;



@RestController
public class TestController {

	@Autowired 
	TestService test;
	
	
	@GetMapping("/{i}")
	public String doc(@PathVariable int i) throws Exception
	{
		return test.testSer(i);
	}
	
}
